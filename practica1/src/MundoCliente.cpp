// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>

#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#define TAM_BUFF 55
#define TAM_CAD 100

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{	
	munmap(org,sizeof(MemC));

	close(fd);
	unlink("/tmp/FIFO_Cliente_Servidor");

	close(fd_teclas);
	unlink("/tmp/FIFO_Teclas");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	int i;
	char buff[TAM_BUFF];
	char buffer_cad[TAM_CAD];

	esfera.Mueve(0.025f);
	esfera.radio-=0.0005;

	//Regulacion del tamano de la pelota
	if(esfera.radio<0.2f) esfera.radio=0.2f;
	if(esfera.radio-0.2f<0.001f && esfera.radio-0.2f>-0.001f) esfera.radio=0.5f;

	//salida del juego por puntos
	if(puntos1==8 || puntos2==8)
		exit(0);
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=3+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=3+2*rand()/(float)RAND_MAX;
		puntos2++;

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-3-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-3-2*rand()/(float)RAND_MAX;
		puntos1++;

	}

	switch(pMemC->accion){
		case 1:  OnKeyboardDown('w',0,0); break;
		case 0:  break;
		case -1: OnKeyboardDown('s',0,0); break;
	}

	//Lectura y gestion de los datos de la tuberia
	read(fd,buffer_cad,sizeof(buffer_cad));
	sscanf(buffer_cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.x2, &jugador1.y1, &jugador1.y2, &jugador2.x1, &jugador2.x2, &jugador2.y1, &jugador2.y2, &puntos1, &puntos2);
	
	pMemC->esfera=esfera;
	pMemC->raqueta1=jugador1;	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[TAM_CAD/2];
	sprintf(cad,"%c",key);
	write(fd_teclas,cad,sizeof(cad));
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-3;break;
	case 'w':jugador1.velocidad.y=3;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}


	
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	MemC.raqueta1=jugador1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//DatosMemCompartida

	//Creacion del fichero donde compartiremos 

	int file=open("/tmp/datosComp.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);

	write(file,&MemC,sizeof(MemC));


	org=(char*)mmap(NULL,sizeof(MemC),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);
	
	pMemC=(DatosMemCompartida*)org;
	
	pMemC->accion=0;
	

	//Tuberia para comunicacion del cliente con el Servidor	
	mkfifo("/tmp/FIFO_Cliente_Servidor",0777);

	fd=open("/tmp/FIFO_Cliente_Servidor",O_RDONLY);

	//Tuberia con informacion de la teclas
	mkfifo("/tmp/FIFO_Teclas",0777);

	fd_teclas=open("/tmp/FIFO_Teclas",O_WRONLY);

	

}

