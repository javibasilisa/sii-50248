#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include "DatosMemCompartida.h"

int main(){
	int fd_fichero;
	DatosMemCompartida *bot1;
	
	fd_fichero = open("/tmp/bot", O_RDWR);
	
  	/* Se proyecta el fichero */
	bot1 = (DatosMemCompartida*)mmap(NULL, sizeof(*(bot1)), PROT_WRITE | PROT_READ, MAP_SHARED, fd_fichero, 0);
	
	
	close(fd_fichero);

	while(1) {
		if(bot1->esfera.radio < 0)
			break;
		//bot
		if(bot1->esfera.centro.y > (bot1->raqueta.y2+(bot1->raqueta.y1-bot1->raqueta.y2)/2.0f))
			bot1->accion = 1;
		else if(bot1->esfera.centro.y < (bot1->raqueta.y2+(bot1->raqueta.y1-bot1->raqueta.y2)/2.0f))
			bot1->accion = -1;
		else
			bot1->accion = 0;
		
		usleep(25000);
	}
	munmap(bot1,sizeof(*(bot1)));
}

